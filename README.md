# Smart Timeshare

## Case: Collective Property Ownership

Your system should allow a group of people to buy and own a property
collectively. Thus, the original seller of the property can sell ownership periods
within a year, which means that a buyer becomes the owner of the property for
the bought period each year. A case for such collectively owned property can be
a house in a touristic location, and the seller will sell 52 weeks of the year. Then,
a buyer can e.g. buy the 23rd week (the beginning of June), and the property is
his for that week each year. Importantly, there should be enough flexibility to
specify various ownership periods for sale, and for the buyers to see the available
option. Note: Since we are operating in the context of Ethereum, all the human
actors are Ethereum accounts that have balance and address. Programs for
both cases are smart contracts, which are Ethereum accounts controlled by the
code.

## Part 1: Formal Specification (30 points)

Produce a simple UML Class Diagram and specify the class invariants, pre-
and post-conditions through OCL. The grading will depend on the ability to
meet the requirements and ingenious use of the Design-by-Contract technique.

Solution:

Uml Class Diagram:

![uml diagram](./docs/Diagram.svg)

OCL definition described in [docs/ocl.pdf](./docs/ocl.pdf):
![](./docs/ocl.pdf)


## Part2: Ethereum Smart Contracts (35 points)

Implement your specification as an Ethereum Smart Contract using Solidity
language.
Note : Ensure that your implementation operates efficiently with the regards
to the notion of Ethereum “Gas”. Your implementation will be graded for its
robustness and efficiency.

Solution:

Smart contract implementation is in 
[smart_timeshare/contracts/Obshaga.sol](./smart_timeshare/contracts/Obshaga.sol)


## Part3: On the Role of Formalism in Requirements (35 points)

1. Read: Read the [survey](https://jmbruel.netlify.app/publication/acm-surveys-2021/) 
article describing different ways to specify requirements 
from natural language to formal and seamless techniques.
2. Fill the [table](https://docs.google.com/spreadsheets/d/18i_uOsEZqqW1cQ8YTUfuByZ3L12ImyIR/edit?usp=sharing&ouid=116525337008257111998&rtpof=true&sd=true): 
There are 5 categories of approaches in the article. Pick
one approach for each category and describe it in the table. The table
should include a short description of the essence of each approach, a brief
personal opinion about it, and some relevant commentary for both the
binary and non-binary criteria. Use your own words (no copy-paste).

Solution:
Filled out table can be found in [docs/Assignment-4-Table.pdf](./docs/Assignment-4-Table.pdf) or
[google doc link](https://docs.google.com/spreadsheets/d/1QqVEtWBf8YMgJzYhDbvLyxdIvcTcdNLG/edit#gid=211588096)

3. Answer the question:: According to the authors, what comprises a
“balanced view” on applying formalisms in requirements and its consequent effect on quality?

Solution:

By balanced view author mentions that the approaches were compared based on a wide set of criteria that are the most critical for different kinds of stakeholders. That way, stakeholders can pick only criteria critical to their project and evaluate approaches based on that. At the same time, the criteria picked are only the top priority once, not to oversaturate the report. This makes it balanced view: many different comparison vectors, but each of the vectors is highly critical to a certain subset of stakeholders.

## Latex
Latex version of this document is in [latex](./latex/) directory.

## Contributors
* Ilya Nokhrin
* Anton Zalaldinov
* Denis Nikolskiy
