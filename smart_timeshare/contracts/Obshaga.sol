// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;


contract CollectivePropertyOwnership {
    
    mapping(bytes32 => Property) public properties;
    mapping(bytes32 => WeekAtProperty) public allWeeks;
    uint constant MAXWEEKS = 52;

    struct Property {
        bytes32 id;
        bytes32 location;
        WeekAtProperty[MAXWEEKS] ownershipByWeek;

    }

    struct WeekAtProperty {
        bytes32 id;
        uint weekNumber;
        uint price;
        address payable owner;
        bytes32 propertyId;
        bool isForSale;
    }


    function getUniqueId() private view returns(bytes32) {
        return bytes32(keccak256(abi.encode(msg.sender)));
    }


    function createProperty(bytes32 location, uint price) public  {
        require(location != "", "location should be non empty");
        
        bytes32 propertyId = getUniqueId();
        WeekAtProperty[MAXWEEKS] memory weeksAtProperty;

        for (uint i = 0; i < MAXWEEKS; i++) {
            bytes32 weekId = getUniqueId();
            weeksAtProperty[i] = (
                // Newly created property weeks belong to the creator
                WeekAtProperty(
                    {
                        id: weekId,
                        weekNumber: i+1,
                        price: price,
                        owner: payable(msg.sender),
                        propertyId: propertyId,
                        isForSale: false
                    }
                )
            );
            // Add new weeks to 'purchased weeks'
            allWeeks[weekId] = weeksAtProperty[i];
        }
        // Add new property to the property map
        properties[propertyId] = Property(
            {
                id: propertyId, 
                location: location,
                ownershipByWeek: weeksAtProperty
            }
        );

     }


    function forSale(bytes32 propertyId, uint weekNumber, uint price) public {
        require(weekNumber < MAXWEEKS, "Week number must be less than 52");
        require(weekNumber > 0, "Week number must be positive");
        require(price > 0, "Price should be positive");
        require(properties[propertyId].id != 0, "Property does not exist");
        require(properties[propertyId].ownershipByWeek[weekNumber].owner == msg.sender, "Can sell only owned weeks");

        properties[propertyId].ownershipByWeek[weekNumber].price = price;
        properties[propertyId].ownershipByWeek[weekNumber].isForSale = true;

    }


    function purchaseWeekAtProperty(bytes32 weekAtPropertyId) public payable {
        require(allWeeks[weekAtPropertyId].owner != msg.sender, "This week already belong to the user");
        require(allWeeks[weekAtPropertyId].isForSale == true, "This week is not for sale");
        require(allWeeks[weekAtPropertyId].price == msg.value, "Can't sell at this price");

        // transfer throws on failure so code below is not executed
        allWeeks[weekAtPropertyId].owner.transfer(msg.value);
        allWeeks[weekAtPropertyId].owner = payable(msg.sender);
        allWeeks[weekAtPropertyId].isForSale = false;
    }


    // Array that contains only weeks for sale for a given property id
    // Array positions not for sale are skipped.
    function viewOnlyWeeksForSale(bytes32 propertyId) public view returns (WeekAtProperty[] memory){
        require(properties[propertyId].id != 0, "Property does not exist");

        WeekAtProperty[] memory result = new WeekAtProperty[](MAXWEEKS);
        for (uint i = 0; i < MAXWEEKS; i++) {
            if (properties[propertyId].ownershipByWeek[i].isForSale){
                result[i] = properties[propertyId].ownershipByWeek[i];
            }
        }
        return result;
    }

}